package methods

import (
	"strings"
	"testing"
	"time"

	models "gitlab.com/dpixel/dp-integration-testing-with-db/models/astro_object"
	"gitlab.com/dpixel/dp-integration-testing-with-db/models/configuration"

	embeddedpostgres "github.com/fergusstrange/embedded-postgres"
	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose/v3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"golang.org/x/exp/slices"
)

const (
	startTimeout              = 15 * time.Second
	LAST_VERSION_WITHOUT_DATA = 2
	LAST_VERSION_WITH_DATA    = 3
)

type testSuite struct {
	suite.Suite
	db *sqlx.DB
}

func (suite *testSuite) SetupTest() {
	if err := goose.UpTo(suite.db.DB, "../migrations/up", LAST_VERSION_WITH_DATA, goose.WithNoVersioning()); err != nil {
		suite.T().Log("Failed to update table to the version with data. Error: ", err.Error())

		return
	}
}

func (suite *testSuite) TearDownTest() {
	if err := goose.DownTo(suite.db.DB, "../migrations/down", LAST_VERSION_WITHOUT_DATA, goose.WithNoVersioning()); err != nil {
		suite.T().Log("Failed to revert table to the clean version. Error: ", err.Error())

		return
	}
}

func TestSuite(test *testing.T) {
	configuration, err := configuration.New("../settings/configuration.json")
	if err != nil {
		test.Errorf("Failed to get the configuration. Error: %s", err.Error())

		return
	}

	clientEmbeddedPostgreSQL := embeddedpostgres.NewDatabase(embeddedpostgres.
		DefaultConfig().
		Port(configuration.GetPort()).
		Version(embeddedpostgres.PostgresVersion(configuration.GetVersion())).
		Database(configuration.GetName()).
		Username(configuration.GetUser()).
		Password(configuration.GetPassword()).
		StartTimeout(startTimeout).
		Logger(nil).
		BinaryRepositoryURL(configuration.GetRepository()),
	)

	if err := clientEmbeddedPostgreSQL.Start(); err != nil {
		test.Errorf("Failed to start PostgreSQL. Error: %s.\n", err.Error())

		return
	}

	defer clientEmbeddedPostgreSQL.Stop()

	testSuite := new(testSuite)
	testSuite.db, err = sqlx.Connect(configuration.GetDriver(), configuration.GetConnectionString())
	if err != nil {
		test.Errorf("Failed to comnect to the DB. Error: %s", err.Error())

		return
	}

	if err := goose.UpTo(testSuite.db.DB, "../migrations/up", LAST_VERSION_WITHOUT_DATA, goose.WithNoVersioning()); err != nil {
		test.Errorf("Failed to execute up goose instructions. Error: %s.\n", err.Error())

		return
	}

	suite.Run(test, testSuite)
}

func (suite *testSuite) TestGettingRowsFromTableIsValid() {
	expect := []models.AstroObject{
		{Name: "Mercury"},
		{Name: "Venus"},
		{Name: "Earth"},
	}
	data := GetRowsFromAstroCatalogueTable(suite.db)

	assert.Equal(suite.T(), len(expect), len(data))
	for _, row := range data {
		assert.Equal(suite.T(), true, slices.Contains(expect, models.AstroObject{Name: row.Name}))
	}
}

func (suite *testSuite) TestGettingRowsFromTableIsInvalid() {
	expect := []models.AstroObject{
		{Name: "Mercury"},
		{Name: "Venus"},
		{Name: "Earth"},
		{Name: "Mars"},
	}
	data := GetRowsFromAstroCatalogueTable(suite.db)

	assert.NotEqual(suite.T(), len(expect), len(data))
}

func (suite *testSuite) TestGettingRowsFromTableIsFailedDueToNonexistingRecord() {
	expect := []models.AstroObject{
		{Name: "Mercury"},
		{Name: "Venus"},
		{Name: "Mars"},
	}
	data := GetRowsFromAstroCatalogueTable(suite.db)

	assert.Equal(suite.T(), len(expect), len(data))
	for _, row := range data {
		if !slices.Contains(expect, models.AstroObject{Name: row.Name}) {
			assert.Equal(suite.T(), false, false)
		}
	}
}

func (suite *testSuite) TestShouldSuccessfullyToGetRowFromAstroCatalogueTableByName() {
	expect := models.AstroObject{Name: "Earth"}
	data := GetRowFromAstroCatalogueTableByName(suite.db, expect.Name)

	assert.Equal(suite.T(), expect.Name, data.Name)
}

func (suite *testSuite) TestShouldSuccessfullyToGetRowFromAstroCatalogueTableByNameInLowerCase() {
	expect := models.AstroObject{Name: "Earth"}
	data := GetRowFromAstroCatalogueTableByName(suite.db, strings.ToLower(expect.Name))

	assert.Equal(suite.T(), expect.Name, data.Name)
}

func (suite *testSuite) TestShouldFailToGetRowFromAstroCatalogueTableByNameDueToAbsenceRecordInTable() {
	expect := models.AstroObject{Name: "Mars"}
	data := GetRowFromAstroCatalogueTableByName(suite.db, expect.Name)

	assert.NotEqual(suite.T(), expect.Name, data.Name)
}

func (suite *testSuite) TestShouldSuccessfullyAddPlanetToAstroCatalogueTable() {
	expect := models.AstroObject{Name: "Mars"}
	data := AddPlanetToAstroCatalogueTable(suite.db, expect.Name)

	assert.NotEqual(suite.T(), true, data)
}

func (suite *testSuite) TestShouldFailToAddPlanetToAstroCatalogueTableDueToAlreadyExistingPlanetInTheTable() {
	expect := models.AstroObject{Name: "Earth"}
	data := AddPlanetToAstroCatalogueTable(suite.db, expect.Name)

	assert.Equal(suite.T(), false, data)
}
