package models

type AstroObject struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
