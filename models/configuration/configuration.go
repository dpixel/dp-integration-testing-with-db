package configuration

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	repoconfiguration "gitlab.com/dpixel/dp-integration-testing-with-db/repositories/configuration"
)

type db struct {
	Driver   string `json:"driver"`
	Host     string `json:"host"`
	Port     uint32 `json:"port"`
	User     string `json:"user"`
	Name     string `json:"name"`
	Password string `json:"password"`
	SSLMode  string `json:"ssl_mode"`
}

type embeddedPostgres struct {
	Repository string `json:"repository"`
	Version    string `json:"version"`
	DB         db     `json:"db"`
}

type configuration struct {
	EmbeddedPostgres embeddedPostgres `json:"embedded-postgres"`
}

func New(path string) (repoconfiguration.Configuration, error) {
	configurationFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer configurationFile.Close()

	bytes, err := io.ReadAll(configurationFile)
	if err != nil {
		return nil, err
	}

	var configuration configuration
	if err := json.Unmarshal(bytes, &configuration); err != nil {
		return nil, err
	}

	return &configuration, nil
}

func (configuration configuration) GetConnectionString() string {
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s", configuration.EmbeddedPostgres.DB.Host, configuration.EmbeddedPostgres.DB.Port, configuration.EmbeddedPostgres.DB.User, configuration.EmbeddedPostgres.DB.Password, configuration.EmbeddedPostgres.DB.Name, configuration.EmbeddedPostgres.DB.SSLMode)
}

func (configuration configuration) GetDriver() string {
	return configuration.EmbeddedPostgres.DB.Driver
}

func (configuration configuration) GetHost() string {
	return configuration.EmbeddedPostgres.DB.Host
}
func (configuration configuration) GetPort() uint32 {
	return configuration.EmbeddedPostgres.DB.Port
}
func (configuration configuration) GetName() string {
	return configuration.EmbeddedPostgres.DB.Name
}
func (configuration configuration) GetPassword() string {
	return configuration.EmbeddedPostgres.DB.Password
}
func (configuration configuration) GetUser() string {
	return configuration.EmbeddedPostgres.DB.User
}
func (configuration configuration) GetRepository() string {
	return configuration.EmbeddedPostgres.Repository
}
func (configuration configuration) GetVersion() string {
	return configuration.EmbeddedPostgres.Version
}
