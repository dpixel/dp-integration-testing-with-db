package configuration

type Configuration interface {
	GetConnectionString() string
	GetHost() string
	GetPort() uint32
	GetName() string
	GetPassword() string
	GetUser() string
	GetRepository() string
	GetVersion() string
	GetDriver() string
}
